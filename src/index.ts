import { ApolloServer } from "apollo-server";
import neo4j from "neo4j-driver";
import { Neo4jGraphQL } from "@neo4j/graphql";

// zugangsdaten zur Datenbank von dem .env file
require("dotenv").config();

import * as fs from "fs";

//importieren des Schemas aus dem .graphql file
const typeDefs = fs
  .readFileSync(require.resolve("./typeDefs.graphql"))
  .toString("utf-8");

//Neo4j driver intialisieren
const driver = neo4j.driver(
  process.env.NEO4J_URI!,
  neo4j.auth.basic(process.env.NEO4J_USER!, process.env.NEO4J_PASSWORD!)
);

//Neo4j GraphQL library initialisieren und Schema & Treiber übergeben
const neo4jGraphQL = new Neo4jGraphQL({ typeDefs, driver });

//Apollo Server starten und port ausgeben
const server = new ApolloServer({
  schema: neo4jGraphQL.schema,
});

void server.listen().then(({ url }) => {
  console.log(`Server is ready a ${url}`);
});
